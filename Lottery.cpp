//
//  Lottery.cpp
//  LotteryNumberGenerator
//
//  Created by Potro on 6/9/19.
//  Copyright © 2019 Potro. All rights reserved.
//

#include <iostream>
#include <cstdlib>
#include <ctime>
#include <string>
#include <vector>
#include <algorithm>
#include <sstream>
#include <limits>
#include <iomanip>
#include "Lottery.h"


Lottery::Lottery()
{
}

Lottery::~Lottery()
{
}

void Lottery::PrintWelcome()
{
    std::cout << std::setw(70) << std::setfill('#') << "" << std::endl;
    std::cout << std::setfill(' ') << std::setw(10) << "";
    std::cout << std::setw(20) << std::left<< "Welcome to California Lottery Random Generator." << std::endl;
    std::cout << std::setw(70) << std::setfill('#') << "" << std::endl;
    //std::cout << "Currently Developing and in Testing Mode." << std::endl;
    std::cout << "\n" << std::endl;
}

void Lottery::TakeUserInput(std::vector<int> &UserDesiredTries)
{
    int* UserInput = new int;
    std::string* TempString = new std::string;
    std::istringstream* StreamInputValidator = new std::istringstream;
    
    for (int i = 0; i < NUMBEROFGAMES; i++)
    {
        std::cout << "Enter the number of " << GAMENAMEARRAY[i];
        std::cout << " numbers to generate " << MAXPLAY << " max: " << std::endl;
        std::cin >> *TempString;
        StreamInputValidator->clear();
        StreamInputValidator->str(*TempString);
        
        if (*StreamInputValidator >> *UserInput)
        {
            if (*UserInput >= 0 && *UserInput <= MAXPLAY)
            {
                UserDesiredTries.push_back(*UserInput);
            }
            else
            {
                std::cout << "Out of Range. Please enter number from 0 to " << MAXPLAY;
                std::cout << "." << std::endl;
                --i;
            }
        }
        else
        {
            std::cout << "Invalid character. Please enter number from 0 to " << MAXPLAY;
            std::cout << "." << std::endl;
            --i;
        }
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
    }
	delete UserInput;
	delete TempString;
	delete StreamInputValidator;
}

void Lottery::GenerateRandomNumber(std::vector<std::vector<int>> &PassedVector, const int &UpperLimit, const int &UserTries)
{
    srand(static_cast<unsigned short>(time(NULL)));
    unsigned short NumberChecker;
    std::vector<int> TempVector {};
    
    PassedVector.resize(UserTries, std::vector<int>(POWERPLAY));
    
    unsigned short Counter {0};
    for (unsigned short i = 0; i < UserTries; ++i)
    {
        for(unsigned short j = 0; j < RANDOMPOWERPLAYMAX; ++j)
        {
            NumberChecker = rand() % UpperLimit;
            if (j > 0)
            {
                for(int Vec : TempVector)
                {
                    if (Vec == NumberChecker)
                    {
                        ++Counter;
                    }
                }
            }
            
            if (Counter == 0 && (j < RANDOMPOWERPLAYMAX))
            {
                if(NumberChecker != 0)
                {
                    TempVector.push_back(NumberChecker);
                    Counter = 0;
                }
            }
            else
            {
                Counter = 0;
            }
        }
        
        std::random_shuffle(TempVector.begin(), TempVector.end());
        PassedVector[i].at(0) = (TempVector[0]);
        TempVector.erase(TempVector.begin(), TempVector.end());
    }
}

void Lottery::GenerateRandomNumbers(const int &UpperLimit, int &UserTries, std::vector<std::vector<int>> &PassedVector)
{
    srand(static_cast<unsigned short>(time(NULL)));
    std::vector<int> TempVector {};
    unsigned short NumberChecker {};
    
    PassedVector.resize(UserTries, std::vector<int>(WHITEBALLMAX));
    unsigned short Counter {0};
    
    for (unsigned short i = 0; i < UserTries; ++i)
    {
        for(unsigned short j = 0; j < RANDOMWHITEMAX; ++j)
        {
            NumberChecker = rand() % UpperLimit;
            if (j > 0)
            {
                for(int Vec : TempVector)
                {
                    if (Vec == NumberChecker)
                    {
                        ++Counter;
                    }
                }
            }
            if (Counter == 0 && (j < RANDOMWHITEMAX))
            {
                if(NumberChecker != 0)
                {
                    TempVector.push_back(NumberChecker);
                    Counter = 0;
                }
            }
            else
            {
                Counter = 0;
            }
        }
        std::random_shuffle(TempVector.begin(), TempVector.end());
        TempVector.erase(TempVector.begin() + 5, TempVector.end());
        std::sort(TempVector.begin(), TempVector.end());
        for (unsigned short k = 0; k < WHITEBALLMAX; ++k)
        {
            PassedVector[i].at(k) = (TempVector[k]);
        }
        TempVector.erase(TempVector.begin(), TempVector.end());
    }
}


void Lottery::PrintGenerationMessage()
{
    std::cout << std::setw(70) << std::setfill('#') << "" << std::endl;
    std::cout << std::setfill(' ') << std::setw(22) << "" << std::left;
    std::cout << "Here are your results." << std::endl;
    std::cout << std::setw(70) << std::setfill('#') << "" << std::endl;
    std::cout << std::setfill(' ');
}

void Lottery::PrintNumbers(std::vector<std::vector<int>> &NumbersArray, std::vector<std::vector<int>> &NumberArray, const int &NameIndex, const int &UserTries)
{
    for (unsigned short i = 0; i < UserTries; ++i)
    {
        std::cout << std::setw(15) << GAMENAMEARRAY[NameIndex];
        for (unsigned short j = 0; j < MAXPLAY; ++j)
        {
            std::cout << std::setw(2) << NumbersArray[i][j] << " ";
        }
        std::cout << std::setw(5) << "" << NumberArray[i][0] << std::endl;
    }
}

void Lottery::PrintExitMessage()
{
    std::cout << std::setw(70) << std::setfill('#') << "" << std::endl;
    std::cout << std::setfill(' ') << std::setw(20) << "";
    std::cout << std::left << "Good Luck!!! Program Exiting." << std::endl;
    std::cout << std::setw(70) << std::setfill('#') << "" << std::endl;
}
