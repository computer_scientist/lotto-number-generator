//
//  SuperLotto.h
//  LotteryNumberGenerator
//
//  Created by Potro on 6/9/19.
//  Copyright © 2019 Potro. All rights reserved.
//

#pragma once
#include <vector>
#include "Lottery.h"

class SuperLotto: public Lottery
{
private:
    const int MAXWHITEBALLNUMBER {47};
    const int MAXMEGANUMBER {27};
    const int NAMEINDEX {2};
protected:
    int UserTries;
    std::vector<std::vector<int>> NumberArray {};
    std::vector<std::vector<int>> MegaNumber {};
public:
    SuperLotto();
    ~SuperLotto();
    void SetUserTries(int&);
    void GenerateRandomNumber();
    void GenerateRandomNumbers();
    void PrintNumbers();
};
