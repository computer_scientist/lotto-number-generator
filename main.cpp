//
//  main.cpp
//  LotteryNumberGenerator
//
//  Created by Potro on 6/9/19.
//  Copyright © 2019 Potro. All rights reserved.
//

#include <vector>
#include "Lottery.h"
#include "PowerBall.h"
#include "MegaMillions.h"
#include "SuperLotto.h"

int main()
{
    std::vector<int> GameTries {};
    
    Lottery* LotteryObject = new Lottery;
	MegaMillions* MegaMillionsObject = new MegaMillions;
	PowerBall* PowerBallObject = new PowerBall;
	SuperLotto* SuperLottoObject = new SuperLotto;
    
	LotteryObject->PrintWelcome();
    LotteryObject->TakeUserInput(GameTries);
    
    MegaMillionsObject->SetUserTries(GameTries[0]);
    PowerBallObject->SetUserTries(GameTries[1]);
    SuperLottoObject->SetUserTries(GameTries[2]);
    
    PowerBallObject->GenerateRandomNumbers();
    MegaMillionsObject->GenerateRandomNumbers();
    SuperLottoObject->GenerateRandomNumbers();
    
    LotteryObject->PrintGenerationMessage();
    PowerBallObject->PrintNumbers();
    MegaMillionsObject->PrintNumbers();
    SuperLottoObject->PrintNumbers();

    delete MegaMillionsObject;
    delete PowerBallObject;
    delete SuperLottoObject;
    
    LotteryObject->PrintExitMessage();
    delete LotteryObject;
    return 0;
}
