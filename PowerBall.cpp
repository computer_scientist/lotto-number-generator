//
//  PowerBall.cpp
//  LotteryNumberGenerator
//
//  Created by Potro on 6/9/19.
//  Copyright © 2019 Potro. All rights reserved.
//

#include "PowerBall.h"

PowerBall::PowerBall()
{
}

PowerBall::~PowerBall()
{
}

void PowerBall::SetUserTries(int &PassedValue)
{
    UserTries = PassedValue;
}

void PowerBall::GenerateRandomNumber()
{
    Lottery::GenerateRandomNumber(PowerBallNumber, POWERBALLMAXNUMBER, UserTries);
}

void PowerBall::GenerateRandomNumbers()
{
    if (UserTries > 0)
    {
        PowerBall::GenerateRandomNumber();
        Lottery::GenerateRandomNumbers(WHITEBALLMAXNUMBER, UserTries, NumberArray);
    }
}

void PowerBall::PrintNumbers()
{
    if (UserTries > 0)
    {
        Lottery::PrintNumbers(NumberArray, PowerBallNumber, NAMEINDEX, UserTries);
    }
}   
