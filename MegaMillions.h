//
//  MegaMillions.h
//  LotteryNumberGenerator
//
//  Created by Potro on 6/9/19.
//  Copyright © 2019 Potro. All rights reserved.
//

#pragma once
#include <vector>
#include "Lottery.h"

class MegaMillions: public Lottery
{
private:
    const int WHITEBALLMAXNUMBER {70};
    const int MEGANUMBERMAX {25};
    const int NAMEINDEX {0};
protected:
    int UserTries {};
    std::vector<std::vector<int>> NumberArray{};
    std::vector<std::vector<int>> MegaNumber{};
public:
    MegaMillions();
    ~MegaMillions();
    void SetUserTries(int&);
    void GenerateRandomNumber();
    void GenerateRandomNumbers();
    void PrintNumbers();
};
