# Calls the Dev Console "vcvarsall" in the cmd
call "%ProgramFiles(x86)%\Microsoft Visual Studio\2019\Community\VC\Auxiliary\Build\vcvarsall.bat" x64

# Compiles the code
cl /EHsc /Fo.\Build\ /Fe.\Build\main.exe *.cpp /link /LIBPATH:"C:\SDL2-2.0.9\lib\x64" /LIBPATH:"C:\glew-2.1.0\lib\Release\x64" SDL2main.lib SDL2.lib SDL2_image.lib SDL2_ttf.lib glew32.lib /SUBSYSTEM:CONSOLE
