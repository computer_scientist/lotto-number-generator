//
//  SuperLotto.cpp
//  LotteryNumberGenerator
//
//  Created by Potro on 6/9/19.
//  Copyright © 2019 Potro. All rights reserved.
//

#include "SuperLotto.h"

SuperLotto::SuperLotto()
{
}

SuperLotto::~SuperLotto()
{
}

void SuperLotto::SetUserTries(int &PassedValue)
{
    UserTries = PassedValue;
}

void SuperLotto::GenerateRandomNumber()
{
    Lottery::GenerateRandomNumber(MegaNumber, MAXMEGANUMBER, UserTries);
}

void SuperLotto::GenerateRandomNumbers()
{
    if (UserTries > 0)
    {
        SuperLotto::GenerateRandomNumber();
        Lottery::GenerateRandomNumbers(MAXWHITEBALLNUMBER, UserTries, NumberArray);
    }
}

void SuperLotto::PrintNumbers()
{
    if (UserTries > 0)
    {
        Lottery::PrintNumbers(NumberArray, MegaNumber, NAMEINDEX, UserTries);
    }
}
