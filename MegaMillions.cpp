//
//  MegaMillions.cpp
//  LotteryNumberGenerator
//
//  Created by Potro on 6/9/19.
//  Copyright © 2019 Potro. All rights reserved.
//

#include "MegaMillions.h"

MegaMillions::MegaMillions()
{
}

MegaMillions::~MegaMillions()
{
}

void MegaMillions::SetUserTries(int &PassedValue)
{
    UserTries = PassedValue;
}

void MegaMillions::GenerateRandomNumber()
{
    Lottery::GenerateRandomNumber(MegaNumber, MEGANUMBERMAX, UserTries);
}

void MegaMillions::GenerateRandomNumbers()
{
    if (UserTries > 0)
    {
        MegaMillions::GenerateRandomNumber();
        Lottery::GenerateRandomNumbers(WHITEBALLMAXNUMBER, UserTries, NumberArray);
    }
}

void MegaMillions::PrintNumbers()
{
    if (UserTries > 0)
    {
        Lottery::PrintNumbers(NumberArray, MegaNumber, NAMEINDEX, UserTries);
    }
}
