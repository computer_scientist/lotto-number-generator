//
//  Lottery.h
//  LotteryNumberGenerator
//
//  Created by Potro on 6/9/19.
//  Copyright © 2019 Potro. All rights reserved.
//

#pragma once
#include <string>

class Lottery
{
private:
    const int WHITEBALLMAX {5};
    const int POWERPLAY {1};
    static const int NUMBEROFGAMES {3};
    const std::string GAMENAMEARRAY[NUMBEROFGAMES] {"Mega Millions", "Power Ball", "Super Lotto"};
protected:
    const int MAXPLAY {5};
    const int RANDOMWHITEMAX {20};
    const int RANDOMPOWERPLAYMAX {5};
public:
    Lottery();
	~Lottery();
    void PrintWelcome();
    void TakeUserInput(std::vector<int>&);
    void GenerateRandomNumber(std::vector<std::vector<int>>&, const int&, const int&);
    void GenerateRandomNumbers(const int&, int&, std::vector<std::vector<int>>&);
    void PrintGenerationMessage();
    void PrintNumbers(std::vector<std::vector<int>>&, std::vector<std::vector<int>>&, const int&, const int&);
    void PrintExitMessage();
};
