//
//  PowerBall.h
//  LotteryNumberGenerator
//
//  Created by Potro on 6/9/19.
//  Copyright © 2019 Potro. All rights reserved.
//

#pragma once
#include <vector>
#include "Lottery.h"

class PowerBall: public Lottery
{
private:
    const int WHITEBALLMAXNUMBER {69};
    const int POWERBALLMAXNUMBER {26};
    const int NAMEINDEX {1};
protected:
    int UserTries {};
    std::vector<std::vector<int>> NumberArray {};
    std::vector<std::vector<int>> PowerBallNumber {};
public:
    PowerBall();
    ~PowerBall();
    void SetUserTries(int&);
    void GenerateRandomNumber();
    void GenerateRandomNumbers();
    void PrintNumbers();
};
